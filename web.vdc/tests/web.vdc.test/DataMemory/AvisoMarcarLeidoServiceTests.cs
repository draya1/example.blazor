﻿using NUnit.Framework;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class AvisoMarcarLeidoServiceTests
    {
        
        private IAvisosMarcarLeidoService _avisosMarcarLeidoService;        

        [SetUp]
        public void Setup()
        {
            
            _avisosMarcarLeidoService = new AvisoMarcarLeidoService();            
        }

       
        [Test]
        public void MarcarLeido_debe_ser_success_true()
        {
            var request = new RequestAvisoMarcarLeido(1, "user");
            _avisosMarcarLeidoService.Request = request;
            
            var result = _avisosMarcarLeidoService.MarcarLeido().GetAwaiter().GetResult();

            Assert.AreEqual(1, result.AvisoId);
            Assert.AreEqual(true, result.Success);
        }

       
    }
}
