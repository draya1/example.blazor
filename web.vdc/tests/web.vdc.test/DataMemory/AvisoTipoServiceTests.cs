﻿using NUnit.Framework;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class AvisoTipoServiceTests
    {
        
        private IAvisoTipoService _avisoTipoService;
        

        [SetUp]
        public void Setup()
        {
        
            _avisoTipoService = new AvisoTipoService();        
        }
        [Test]
        public void GetTiposAvisos_debe_obtener_tipos()
        {
            var result = _avisoTipoService.GetTiposAvisosAsync().GetAwaiter().GetResult();

            Assert.AreEqual($"1 Inmediato", $"{result[0].Id} {result[0].Descripcion}");
            Assert.AreEqual($"2 Programado", $"{result[1].Id} {result[1].Descripcion}");
            Assert.AreEqual($"3 Inicio Sesión", $"{result[2].Id} {result[2].Descripcion}");
            Assert.AreEqual($"4 Inicio Sesión Repetición", $"{result[3].Id} {result[3].Descripcion}");
        }

        
    }
}
