﻿using NUnit.Framework;
using Web.Vdc.Doctor.DataMemory;
using Web.Vdc.Doctor.Services;

namespace web.vdc.test.DataMemory
{
    public class AvisoCategoriasServiceTests
    {
        private IAvisoCategoriaService _avisoCategoriaService;

        [SetUp]
        public void Setup()
        {
            _avisoCategoriaService = new AvisoCategoriaService();
        }

        [Test]
        public void GetCategoriasAviso_debe_obtener_categorias()
        {

            var result = _avisoCategoriaService.GetCategoriasAvisosAsync().GetAwaiter().GetResult();

            Assert.AreEqual("Informativo", result[0].Descripcion);
            Assert.AreEqual("Alerta", result[1].Descripcion);
            Assert.AreEqual("Importante", result[2].Descripcion);

            Assert.AreEqual(1, result[0].Id);
            Assert.AreEqual(2, result[1].Id);
            Assert.AreEqual(3, result[2].Id);

        }


    }
}
