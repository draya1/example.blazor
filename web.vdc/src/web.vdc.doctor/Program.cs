using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;

namespace Web.Vdc.Doctor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // The initial "bootstrap" logger is able to log errors during start-up. It's completely replaced by the
            // logger configured in `UseSerilog()` below, once configuration and dependency-injection have both been
            // set up successfully.
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
                

            try
            {
                #region Syncfusion
                var keySyncfusion = "NDQzNzE3QDMxMzkyZTMxMmUzMFJVTUhzUE95V1ZOcFI5K0FnZmJ4OEdoRHMraExGMFhXeFFrcmxQT0N0b3c9"; //Syncfusion 19.1.x                
                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(keySyncfusion);
                #endregion

                var host = CreateHostBuilder(args).Build();

                Log.Information($"Starting host...");

                host.Run();

            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host exception.");
            }
            finally
            {
                Log.CloseAndFlush();
            }       

            //CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((context, services, configuration) => configuration
                    .ReadFrom.Configuration(context.Configuration)
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext()
                    .WriteTo.Console())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseSerilog();
                });
    }
}
