using Blazor.FileReader;
using Blazored.Modal;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Syncfusion.Blazor;
using System;
using Web.Vdc.Doctor.Dependencyinjection;

namespace Web.Vdc.Doctor
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            
            var api = Configuration.GetSection("ApiServer:vdc");
            var memory = Configuration.GetSection("DataMemory:active");
            var RefreshIntervalAvisosSetting = Configuration.GetSection("RefreshIntervalAvisos:miliseconds");
            double.TryParse(RefreshIntervalAvisosSetting.Value ?? "60000", out double refreshAvisos); //por defecto si no hay setting, se establece en 1 minuto (60000 ms)

            

            services.AddHttpContextAccessor();

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddServerSideBlazor().AddCircuitOptions(options => { options.DetailedErrors = true; });
            services.AddServerSideBlazor().AddHubOptions(o =>
            {
                o.MaximumReceiveMessageSize = 102400000;
            });


            services.AddFileReaderService();
            services.AddBlazoredModal();
            services.AddSyncfusionBlazor();


            if (memory.Value == "true")
            {
                services.AddDomainServices(true, refreshAvisos);
            }
            else
            {
                services.AddDomainServices(false, refreshAvisos);
                services.AddHttpClient("vdc.api", c =>
                {
                    c.BaseAddress = new Uri(api.Value);
                    c.DefaultRequestHeaders.Add("Accept", "application/json");
                    c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Web.Vdc.Doctor");

                });               
            }

            //localization
            services.AddLocalizationServices();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            #region Localization
            var supportedCultures = new[] { "es-ES" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);
            app.UseRequestLocalization(localizationOptions);
            #endregion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();            
            
            
            app.UseRouting();

           


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }


       
    }
}
