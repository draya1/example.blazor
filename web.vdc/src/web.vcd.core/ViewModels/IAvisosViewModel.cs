﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;

namespace Web.Vdc.Doctor.ViewModels
{
    public interface IAvisoViewModel
    {   
        public bool IsBusy { get; set; }
        public ModelState State { get; set; }
        public List<AvisoCategoria> CategoriasList { get; }
        public List<AvisoTipo> TiposList { get; }
        public UsuarioTreeItem UsuariosList { get; }
        public List<AvisoBase> AvisosList { get; }
        public Aviso AvisoItem { get; set; }
        public Aviso AvisoClone { get; set; }
        
        event PropertyChangedEventHandler PropertyChanged;
        
        public Task LoadModel();
        public Task SetItem(int id);
        public Task ChangeStateItem(int id, bool state);
        public Task SaveChanges();
        public Task CancelChanges();
        public Task DuplicateItem(int sourceId);
        

    }
}
