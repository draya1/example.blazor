﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.ViewModels
{
    public class AvisoViewModel : BaseViewModel, IAvisoViewModel
    {
        private readonly IAvisoService _service;
        public AvisoViewModel(IAvisoService service)
        {
            _service = service;
        }

        private Aviso avisoItem = new Aviso();
        public Aviso AvisoItem { get => avisoItem; set => SetValue(ref avisoItem, value); }

        private List<AvisoBase> avisosList = new List<AvisoBase>();

        public List<AvisoBase> AvisosList { get => avisosList; private set => SetValue(ref avisosList, value); }
        
        public List<AvisoCategoria> CategoriasList { get; private set; } = new List<AvisoCategoria>();
        public List<AvisoTipo> TiposList { get; private set; } = new List<AvisoTipo>();
        public UsuarioTreeItem UsuariosList { get; private set; } = new UsuarioTreeItem();
        public Aviso AvisoClone { get; set; }

        public async Task LoadModel()
        {

            IsBusy = true;
            
            CategoriasList = await _service.SourceCategorias;
            TiposList = await _service.SourceTipos;
            
            var locUsuarios = await _service.SourceUsuarios;
            UsuariosList = new UsuarioTreeItem(locUsuarios);

            avisosList = await _service.Source;

            State = ModelState.Browsing;
            
            IsBusy = false;

        }

        public async Task ChangeStateItem(int id, bool state)
        {
            IsBusy = true;
            
            await _service.CambiarEstado(id, state);
            
            var localItem = avisosList.FirstOrDefault(p => p.Id == id);
            if (localItem != null)
            {
                localItem.Activo = state;
                OnPropertyChanged(nameof(avisosList));
            }

            State = ModelState.Browsing;

            IsBusy = false;
        }

        public async Task SetItem(int id)
        {
            IsBusy = true;

            if (id == 0)
            {
                avisoItem = new Aviso();
                State = ModelState.Creating;

            }
            else
            {
                avisoItem = await _service.GetAsync(id);                
                State = ModelState.Browsing;
            }

            UsuariosList.SetState(avisoItem.AvisosUsuarios.Select(s => s.UserName).ToList());

            AvisoClone = avisoItem.DeepClone();
            

            IsBusy = false;
        }

        public async Task CancelChanges()
        {
            IsBusy = true;
            
            avisoItem = await Task.Run( ()=> this.AvisoClone.DeepClone());

            State = ModelState.Browsing;

            IsBusy = false;

            
        }

        public async Task SaveChanges()
        {
            IsBusy = true;

            if (avisoItem.Id == 0)
            {
                avisoItem.FechaCreacion = DateTime.Now;
                avisoItem.Activo = true;                
                if (avisoItem.AvisoTipoId == 1)
                {
                    avisoItem.FechaDesde = avisoItem.FechaCreacion.Value.AddMinutes(-5);  
                    avisoItem.FechaHasta = avisoItem.FechaCreacion.Value.AddMinutes(5);  
                }

                AvisoClone = avisoItem.DeepClone();

                avisoItem.Id = await _service.CreateAsync(avisoItem);

                avisosList.Add(new AvisoBase
                {
                    Id = avisoItem.Id,
                    Titulo = avisoItem.Titulo,
                    Activo = avisoItem.Activo,
                    FechaCreacion = AvisoClone.FechaCreacion,
                    FechaDesde = AvisoClone.FechaDesde,
                    FechaHasta = AvisoClone.FechaHasta,
                    AvisoCategoriaId = avisoItem.AvisoCategoriaId,                    
                    AvisoTipoId = avisoItem.AvisoTipoId,
                    Categoria = this.CategoriasList.FirstOrDefault(p => p.Id == avisoItem.AvisoCategoriaId)?.Descripcion,
                    Tipo = this.TiposList.FirstOrDefault(p => p.Id == avisoItem.AvisoTipoId)?.Descripcion                    
                });
            }
            else
            {
                await _service.CambiarEstado(avisoItem.Id, avisoItem.Activo);
            }

            OnPropertyChanged(nameof(avisosList));

            State = ModelState.Browsing;

            IsBusy = false;
        }

        public async Task DuplicateItem(int sourceId)
        {
            await this.SetItem(sourceId);

            IsBusy = true;
            
            //update id's from source item
            avisoItem.Id = 0;
            avisoItem.FechaCreacion = DateTime.Now;
            avisoItem.AvisosUsuarios.ForEach(p => { p.Id = 0; p.AvisoId = 0; p.FechaLeido = null; });
            //update dates aviso inmediato
            if (avisoItem.AvisoTipoId == 1)
            {
                avisoItem.FechaDesde = DateTime.Now.AddMinutes(-5);
                avisoItem.FechaHasta = DateTime.Now.AddMinutes(5);
            }

            State = ModelState.Creating;

            IsBusy = false;
            
            


            
        }
    }
}
