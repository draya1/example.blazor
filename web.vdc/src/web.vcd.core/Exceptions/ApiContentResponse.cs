﻿using System;

namespace Web.Vdc.Doctor.Exceptions
{
    [Serializable]
    public class ApiContentResponse
    {
        //{"title":"Bad Request","status":400,"detail":"Message information/error."}
        public string Title { get; set; }

        public int Status { get; set; }

        public string Detail { get; set; }

        public string error { get; set; }

    }


}
