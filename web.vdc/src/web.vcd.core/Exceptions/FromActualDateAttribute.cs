﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Vdc.Doctor.Exceptions
{
    public class FromActualDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime fecha;

            if (!DateTime.TryParse(value.ToString(), out fecha))
            {
                throw new ArgumentException("The FromActualDateAttribute, only validate DateTime Types.");
            }

            return fecha.Date >= DateTime.Now.Date;
        }
    }
}
