﻿using Syncfusion.Blazor;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;

namespace Web.Vdc.Doctor.Services
{
    public interface IMasterDataAdaptor<TEntityBase, TEntityItem>
         where TEntityBase : MaestroBase
         where TEntityItem : ModelBase
    {

        
        object Read(DataManagerRequest dataManagerRequest, string key = null);
        Task<object> ReadAsync(DataManagerRequest dataManagerRequest, string key = null);

    }
    
}
