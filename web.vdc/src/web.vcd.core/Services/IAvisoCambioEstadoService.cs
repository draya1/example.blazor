﻿using System.Threading.Tasks;

namespace Web.Vdc.Doctor.Services
{
    public interface IAvisoCambioEstadoService
    {
        Task CambiarEstado(int avisoId, bool estado);
    }
}