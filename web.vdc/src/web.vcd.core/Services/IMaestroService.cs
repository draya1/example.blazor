﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;

namespace Web.Vdc.Doctor.Services
{
    public interface IMaestroService<TEntityBase, TEntityItem> where TEntityBase : MaestroBase where TEntityItem : ModelBase
    {
        public ParamRequest Params { get => Params ?? new ParamRequest(); set => Params = value ?? new ParamRequest(); }
        Task<List<TEntityBase>> Source { get; }
        Task<int> CreateAsync(TEntityItem item);        
        Task<TEntityItem> GetAsync(int id);
        Task UpdateAsync(TEntityItem item);
    }

}
