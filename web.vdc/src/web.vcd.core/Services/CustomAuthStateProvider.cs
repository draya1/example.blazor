﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;

public class CustomAuthStateProvider : AuthenticationStateProvider
{    

    public override Task<AuthenticationState> GetAuthenticationStateAsync()
    {

        
        var identity = new ClaimsIdentity(new[]
        {
            new Claim(ClaimTypes.Name, "master"),
            new Claim(ClaimTypes.Role, "admin")
        }, "Fake authentication type");



        var user = new ClaimsPrincipal(identity);
        

        return Task.FromResult(new AuthenticationState(user));
    }
    public void NotifyAuthenticationStateChanged()
    {
        NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
    }
}