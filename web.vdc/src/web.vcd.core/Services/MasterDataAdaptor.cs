﻿using Syncfusion.Blazor;
using Syncfusion.Blazor.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;

namespace Web.Vdc.Doctor.Services
{

    public class MasterDataAdaptor<TEntityBase, TEntityItem> : IMasterDataAdaptor<TEntityBase, TEntityItem>
        where TEntityBase : MaestroBase
        where TEntityItem : ModelBase

    {
        protected readonly IMaestroService<TEntityBase, TEntityItem> _service;

        public MasterDataAdaptor(IMaestroService<TEntityBase, TEntityItem> service)
        {
            _service = service;
        }

        public object Read(DataManagerRequest dataManagerRequest, string key = null)
        {

            if (_service.Params.ApiQueryString)
            {
                return ReadApi(dataManagerRequest, key);
            }
            else
            {
                return ReadMemory(dataManagerRequest, key);
            }

        }

        public Task<object> ReadAsync(DataManagerRequest dataManagerRequest, string key = null)
        {
            return Task.FromResult(Read(dataManagerRequest, key));
        }


        public object ReadMemory(DataManagerRequest dataManagerRequest, string key = null)
        {
            IEnumerable<TEntityBase> DataSource = _service.Source.GetAwaiter().GetResult();

            if (dataManagerRequest.Search != null && dataManagerRequest.Search.Count > 0)
            {
                // Searching
                DataSource = DataOperations.PerformSearching(DataSource, dataManagerRequest.Search);
            }
            if (dataManagerRequest.Sorted != null && dataManagerRequest.Sorted.Count > 0)
            {
                // Sorting
                DataSource = DataOperations.PerformSorting(DataSource, dataManagerRequest.Sorted);
            }

            if (dataManagerRequest.Where != null) dataManagerRequest.Where.RemoveAll(r => r.Field == "ChildId");
            if (dataManagerRequest.Where != null && dataManagerRequest.Where.Count > 0)
            {                
                // Filtering
                DataSource = DataOperations.PerformFiltering(DataSource, dataManagerRequest.Where, dataManagerRequest.Where[0].Operator);
            }
            int count = DataSource.Cast<TEntityBase>().Count();
            if (dataManagerRequest.Skip != 0)
            {
                //Paging
                DataSource = DataOperations.PerformSkip(DataSource, dataManagerRequest.Skip);
            }
            if (dataManagerRequest.Take != 0)
            {
                DataSource = DataOperations.PerformTake(DataSource, dataManagerRequest.Take);
            }
            return dataManagerRequest.RequiresCounts ? new DataResult() { Result = DataSource, Count = count } : (object)DataSource;
        }


        public object ReadApi(DataManagerRequest dataManagerRequest, string key = null)
        {
            var serviceParams = new ParamRequest();

            serviceParams.ApiQueryString = true;

            if (dataManagerRequest.Where != null && dataManagerRequest.Where.Count > 0 && dataManagerRequest.Where.ToList().Any(a => a.value != null))
            {
                foreach (var where in dataManagerRequest.Where)
                {
                    if (where.Field == "ParentId")
                    {
                        serviceParams.WhereParentContent = where.value?.ToString() ?? string.Empty;
                    }
                    else if (where.Field == "ChildId")
                    {
                        serviceParams.WhereChildContent = where.value?.ToString() ?? string.Empty;
                    }
                    else
                    {
                        serviceParams.WhereContent = where.value?.ToString() ?? string.Empty;
                    }
                }
            }

            if (dataManagerRequest.Search != null && dataManagerRequest.Search.Count > 0) serviceParams.SearchContent = dataManagerRequest.Search.FirstOrDefault().Key ?? string.Empty;
            if (dataManagerRequest.Sorted != null && dataManagerRequest.Sorted.Count > 0)
            {
                serviceParams.OrderDirection = dataManagerRequest.Sorted.FirstOrDefault().Direction ?? string.Empty;
                serviceParams.OrderColumnName = dataManagerRequest.Sorted.FirstOrDefault().Name ?? string.Empty;
            }
            if (dataManagerRequest.Skip != 0) serviceParams.SkipRows = dataManagerRequest.Skip;
            if (dataManagerRequest.Take != 0) serviceParams.TakeRows = dataManagerRequest.Take;

            _service.Params = serviceParams;

            IEnumerable<TEntityBase> DataSource = _service.Source.GetAwaiter().GetResult();


            return dataManagerRequest.RequiresCounts ? new DataResult() { Result = DataSource, Count = serviceParams.Count } : (object)DataSource;
        }

    }


}
