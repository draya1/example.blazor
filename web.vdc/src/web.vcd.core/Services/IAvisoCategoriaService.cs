﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;

namespace Web.Vdc.Doctor.Services
{
    public interface IAvisoCategoriaService
    {
        Task<List<AvisoCategoria>> GetCategoriasAvisosAsync();
    }
}