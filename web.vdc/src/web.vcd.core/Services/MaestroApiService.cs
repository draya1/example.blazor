﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;

namespace Web.Vdc.Doctor.Services
{
    public class MaestroApiService<TEntityBase, TEntityItem> where TEntityBase : MaestroBase where TEntityItem : ModelBase
    {

        private readonly IHttpClientFactory _clientFactory;
        private readonly string apiClient;
        private readonly string apiResource;
        private readonly string itemName;

        public MaestroApiService(IHttpClientFactory clientFactory, string apiClient, string apiResource, string itemName)
        {
            _clientFactory = clientFactory;
            this.apiClient = apiClient;
            this.apiResource = apiResource;
            this.itemName = itemName;
        }


        public ParamRequest Params { get; set; } = new ParamRequest();

        public Task<List<TEntityBase>> Source => GetSourceAsync();

        public virtual async Task<int> CreateAsync(TEntityItem item)
        {
            var client = _clientFactory.CreateClient(apiClient);
            var stringPayload = await Task.Run(() => JsonSerializer.Serialize<TEntityItem>(item));

            var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");
            var response = client.PostAsync(apiResource, message).GetAwaiter().GetResult();

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            var dataResponse = GetContentResponseAsString(response).GetAwaiter().GetResult();

            int.TryParse(dataResponse, out int result);
            return result;

        }

        public virtual async Task<TEntityItem> GetAsync(int id)
        {
            TEntityItem result;

            var client = _clientFactory.CreateClient(apiClient);
            var request = new HttpRequestMessage(HttpMethod.Get, $"{apiResource}/Get{itemName}/{id}");
            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                result = await JsonSerializer.DeserializeAsync
                        <TEntityItem>(responseStream);
            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result;
        }

        public async Task UpdateAsync(TEntityItem item)
        {
            var client = _clientFactory.CreateClient(apiClient);
            var stringPayload = await Task.Run(() => JsonSerializer.Serialize<TEntityItem>(item));

            var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");
            var response = client.PutAsync(apiResource, message).GetAwaiter().GetResult();

            if (!response.IsSuccessStatusCode)
            {

                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }
        }

        protected virtual async Task<List<TEntityBase>> GetSourceAsync()
        {

            IEnumerable<TEntityBase> result;
            var request = new HttpRequestMessage(HttpMethod.Get, apiResource + GetQueryStyring());
            var client = _clientFactory.CreateClient(apiClient);

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                result = await GetListResult(responseStream);

            }
            else
            {
                throw new ApiException($"{GetConentResponse(response).GetAwaiter().GetResult().Detail}");
            }

            return result.ToList();

        }

        
        protected virtual async Task<string> GetContentResponseAsString(HttpResponseMessage response)
        {
            var result = string.Empty;

            try
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                var txtReader = new System.IO.StreamReader(responseStream);
                var reader = new Newtonsoft.Json.JsonTextReader(txtReader);
                var serializer = new Newtonsoft.Json.JsonSerializer();

                

                var data = serializer.Deserialize<Newtonsoft.Json.Linq.JObject>(reader);
                if (data != null)
                {
                    result = data.First.ToString().Split(":")[1];                    
                }

            }
            catch { }

            return result;
        }
        
        protected virtual async Task<ApiContentResponse> GetConentResponse(HttpResponseMessage response)
        {
            var result = new ApiContentResponse();

            try
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                var txtReader = new System.IO.StreamReader(responseStream);
                var reader = new Newtonsoft.Json.JsonTextReader(txtReader);
                var serializer = new Newtonsoft.Json.JsonSerializer();

                var data = serializer.Deserialize<ApiContentResponse>(reader);
                if (data != null)
                {
                    result.Status = data.Status;
                    result.Title = data.Title;
                    result.Detail = data.Detail;
                }

            }
            catch { }

            return result;


        }

        protected virtual string GetQueryStyring()
        {

            if (this.Params.ApiQueryString is false) return string.Empty;


            var desc = Params.OrderDirection == "descending" ? "-" : string.Empty;            
            return $"?$inlinecount=4&$skip={Params.SkipRows}&$top={Params.TakeRows}&$search={Params.SearchContent}&$order={desc}{Params.OrderColumnName}&$where={Params.WhereContent}&$whereParent={Params.WhereParentContent}&$whereChild={Params.WhereChildContent}&$whereNodo={Params.WhereNodo}";
            
        }

        protected async Task<IEnumerable<TEntityBase>> GetListResult(System.IO.Stream responseStream)
        {

            IEnumerable<TEntityBase> result;


            if (Params.ApiQueryString)
            {
                var data = await JsonSerializer.DeserializeAsync<MaestroBaseList<TEntityBase>>(responseStream);
                result = data.Items;
                Params.Count = data.Count;
            }
            else
            {
                var data = await JsonSerializer.DeserializeAsync<IEnumerable<TEntityBase>>(responseStream);
                result = data;                
            }
            
            
            
            return result;

        }
    }
}
