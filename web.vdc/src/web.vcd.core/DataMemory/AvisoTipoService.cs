﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.Avisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.DataMemory
{
    public class AvisoTipoService : IAvisoTipoService
    {
        private readonly List<AvisoTipo> AvisosTipos = new List<AvisoTipo>()
        {
            new AvisoTipo {Id = 1, Descripcion = "Inmediato"},
            new AvisoTipo {Id = 2, Descripcion = "Programado"},
            new AvisoTipo {Id = 3, Descripcion = "Inicio Sesión" },
            new AvisoTipo {Id = 4, Descripcion = "Inicio Sesión Repetición" }
        };

        public async Task<List<AvisoTipo>> GetTiposAvisosAsync()
        {
            return await Task.FromResult(AvisosTipos);
        }
    }

}
