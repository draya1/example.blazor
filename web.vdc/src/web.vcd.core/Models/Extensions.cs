﻿using System.Text.Json;

namespace Web.Vdc.Doctor.Models
{
    public static class Extensions
    {
        public static T DeepClone<T>(this T obj)
        {

            var aux = JsonSerializer.Serialize<T>(obj);

            return JsonSerializer.Deserialize<T>(aux);

        }

    }
}
