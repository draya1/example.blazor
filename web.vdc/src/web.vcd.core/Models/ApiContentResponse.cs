﻿using System;

namespace Web.Vdc.Doctor.Models
{
    [Serializable]
    public class ApiContentResponse
    {
        //{"title":"Bad Request","status":400,"detail":"El nombre 'Tipo Poliza 01' ya existe para otro tipo de póliza."}
        public string Title { get; set; }

        public int Status { get; set; }

        public string Detail { get; set; }
        public object Errors { get; set; }

    }


}
