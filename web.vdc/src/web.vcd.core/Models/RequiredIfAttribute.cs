﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Vdc.Doctor.Models
{
    public class RequiredIfAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessageFormatString = "The {0} field is required.";
        private readonly string[] _dependentProperties;

        public RequiredIfAttribute(string[] dependentProperties)
        {
            _dependentProperties = dependentProperties;
            ErrorMessage = DefaultErrorMessageFormatString;
        }

        protected override ValidationResult IsValid(Object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();

            foreach (string s in _dependentProperties)
            {
                Object propertyValue = type.GetProperty(s).GetValue(instance, null);
                if ((bool)propertyValue == false)
                {
                    return ValidationResult.Success;
                }

                if ((bool)propertyValue == true && value != null)
                {
                    return ValidationResult.Success;
                }
            }
            return new ValidationResult(context.DisplayName + " required. ");
        }
    }

}
