﻿namespace Web.Vdc.Doctor.Models.Avisos
{
    public class AvisoCategoria
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
