﻿using System;

namespace Web.Vdc.Doctor.Models.Avisos
{
    public class AvisoUsuario
    {
        public int Id { get; set; }
        public int? AvisoId { get; set; }
        public string UserName { get; set; }
        public DateTime? FechaLeido { get; set; }
    }
}