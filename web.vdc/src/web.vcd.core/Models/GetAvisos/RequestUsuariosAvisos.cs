﻿using System;

namespace Web.Vdc.Doctor.Models.GetAvisos
{
    public interface IRequest { }

    public interface IRequestUsuariosAvisos : IRequest
    {
        RequestUsuariosAvisos Request { get; set; }
    }

    public class RequestUsuariosAvisos
    {
        public RequestUsuariosAvisos(string userName, bool esInicioSesion, DateTime? fechaActual)
        {
            UserName = userName;
            EsInicioSesion = esInicioSesion;
            FechaActual = fechaActual;
        }

        public string UserName { get; private set; }
        public bool EsInicioSesion { get; private set; }
        public DateTime? FechaActual { get; private set; }
    }

}
