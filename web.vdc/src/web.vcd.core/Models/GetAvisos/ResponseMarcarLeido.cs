﻿namespace Web.Vdc.Doctor.Models.GetAvisos
{
    public class ResponseMarcarLeido
    {
        public int AvisoId { get; set; }
        public bool Success { get; set; }
    }
}
