﻿using System;

namespace Web.Vdc.Doctor.Models.GetAvisos
{
    public enum AvisoTipo
    {
        Inmediato = 1,
        Programado = 2,
        InicioSesion = 3,
        InicioSesionRepeticion = 4
    }

    public enum AvisoCategoria
    {
        Informativo = 1,
        Alerta = 2,
        Importante = 3
    }

    public class ToastAviso
    {
        public ToastAviso()
        {
        }

        public ToastAviso(int id, string titulo, string texto, int categoriaId, int tipoId, DateTime fechaDesde, DateTime fechaHasta, DateTime? fechaLeido, bool activo, string userName)
        {
            Id = id;
            Titulo = titulo;
            Texto = texto;
            CategoriaId = categoriaId;
            TipoId = tipoId;
            FechaCreacion = DateTime.Now;
            FechaDesde = fechaDesde;
            FechaHasta = fechaHasta;
            FechaLeido = fechaLeido;
            Activo = activo;
            UserName = userName;
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public int CategoriaId { get; set; }
        public int TipoId { get; set; }
        public DateTime FechaCreacion { get; private set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public DateTime? FechaLeido { get; set; }
        public bool Activo { get; set; }
        public string UserName { get; set; }

    }
}
