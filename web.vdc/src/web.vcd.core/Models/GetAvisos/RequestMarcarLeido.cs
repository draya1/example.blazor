﻿
namespace Web.Vdc.Doctor.Models.GetAvisos
{
    public interface IRequestAvisosMarcarLeido : IRequest
    {
        RequestAvisoMarcarLeido Request { get; set; }
    }

    public class RequestAvisoMarcarLeido
    {
        public RequestAvisoMarcarLeido(int avisoId, string user)
        {
            AvisoId = avisoId;
            User = user;
        }

        public int AvisoId { get; private set; }
        public string User { get; private set; }
    }
}
