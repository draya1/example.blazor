﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Vdc.Doctor.Models
{
    
    public class ModelBase
    {
        [Key]
        public int Id { get; set; }

    }

    public class MaestroBase : ModelBase
    {        
        [Display(Name = "Nombre", ShortName = "Nombre")]
        [MaxLength(50, ErrorMessage = "'{0}' debe tener una longitud máxima de 50 caracteres.")]
        [Required(ErrorMessage = "'{0}' es requerido")]
        public string Nombre { get; set; }
    }

    public class MaestroBaseList<TEntityBase> where TEntityBase : MaestroBase
    {
        public IEnumerable<TEntityBase> Items { get; set; }

        public int Count { get; set; }

    }

}
