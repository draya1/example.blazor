﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models.GetAvisos;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.Data
{
    public class GetAvisosService : IGetAvisosService, IDisposable
    {
        private readonly IUsuariosAvisosService _avisos;
        public RequestUsuariosAvisos Request { get; set; }
        public List<ToastAviso> AvisosPendientes { get; set; }

        public event Action OnChange;
        private void NotifyDataChanged() => OnChange?.Invoke();

        private readonly ITimerService _timer;

        public GetAvisosService(IUsuariosAvisosService avisosService, ITimerService timer, double refreshInterval)
        {
            AvisosPendientes = new List<ToastAviso>();

            _avisos = avisosService;

            _timer = timer;
            _timer.SetTimer(refreshInterval); // defecto: 300000ms -> 300 segundos -> 5 minutos
            _timer.OnElapsed += GetAvisosPendientes;
        }

        public async void GetAvisosPendientes()
        {
            try
            {
                if (Request == null) throw new Exception("RequestUsuariosAvisos is null");

                _avisos.Request = new RequestUsuariosAvisos(Request.UserName, Request.EsInicioSesion, Request.FechaActual);

                var response = await _avisos.GetAviso();

                if (response != null)
                {
                    foreach (var aviso in response)
                    {
                        if (!AvisosPendientes.Any(a => a.Id == aviso.Id))
                        {
                            AvisosPendientes.Add(aviso);
                        }
                    }

                    await Task.Delay(1);
                    NotifyDataChanged();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.OnElapsed -= GetAvisosPendientes;
                _timer.Dispose();
            }
        }
    }

}
