﻿using System;
using System.Timers;

namespace Web.Vdc.Doctor.Data
{
    public interface ITimerService
    {
        event Action OnElapsed;

        void Dispose();
        void SetTimer(double interval);
    }

    public class TimerService : IDisposable, ITimerService
    {
        private Timer _timer;

        public void SetTimer(double interval)
        {
            _timer = new Timer(interval);
            _timer.Elapsed += NotifyTimerElapsed;
            _timer.Enabled = true;
        }

        public event Action OnElapsed;

        private void NotifyTimerElapsed(object source, ElapsedEventArgs e)
        {
            OnElapsed?.Invoke();
        }

        public void Dispose()
        {
            if (_timer != null) _timer.Dispose();
        }
    }
}
