﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Services;
using System.Linq;
using Web.Vdc.Doctor.Models.GetAvisos;

namespace Web.Vdc.Doctor.Data
{
    public class UsuariosAvisosApiService : BaseApiService<RequestUsuariosAvisos, ToastAviso>, IUsuariosAvisosService
    {
        public UsuariosAvisosApiService(IHttpClientFactory clientFactory)
            : base(clientFactory, "vdc.api", "Avisos", "UsuariosAvisos")
        {
        }

        public async Task<List<ToastAviso>> GetAviso()
        {
            return (await ExecuteGetIEnumerableAsync()).ToList();
        }

        protected override string GetQueryString()
        {
            var fechaActual = this.Request.FechaActual != null ? $"&fechaActual={this.Request.FechaActual}" : "";

            var query = $"?user={this.Request.UserName}&sesion={this.Request.EsInicioSesion}" + fechaActual;

            return query;
        }

    }
}
