﻿using System.Net.Http;
using System.Threading.Tasks;
using Web.Vdc.Doctor.Models;
using Web.Vdc.Doctor.Services;

namespace Web.Vdc.Doctor.Data
{

    public class AvisoCambioEstadoApiService : IAvisoCambioEstadoService
    {
        private readonly IHttpClientFactory _clientFactory;

        public AvisoCambioEstadoApiService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public Task CambiarEstado(int avisoId, bool estado)
        {
            var client = _clientFactory.CreateClient("vdc.api");
            var stringPayload = string.Empty;

            var message = new StringContent(stringPayload, System.Text.Encoding.UTF8, "application/json");
            var response = client.PatchAsync($"avisos/cambiarEstado?avisoId={avisoId}&estado={estado}", message).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode == false)
            {
                throw new ApiException($"Api Error: {response.StatusCode} - {response.Content.ToString()}");
            }

            return Task.FromResult(response);
        }
    }

   
}
