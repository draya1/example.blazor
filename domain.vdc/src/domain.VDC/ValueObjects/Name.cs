﻿using System;

namespace Domain.VDC.ValueObjects
{
    public struct Name
    {
        private readonly string _text;

        public Name(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) throw new NameValidationException("The 'Name' is required.");

            if (text.Length > 50) throw new NameValidationException("The 'Name' supports a maximum of 50 characters.");

            _text = text;
        }

        public override string ToString() => _text;
        

    }
}
