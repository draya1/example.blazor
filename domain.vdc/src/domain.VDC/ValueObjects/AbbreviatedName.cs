﻿namespace Domain.VDC.ValueObjects
{

    public readonly struct AbbreviatedName
    {

        private readonly string _text;

        public AbbreviatedName(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) throw new NameValidationException("The 'Name' is required.");

            if (text.Length > 15) throw new NameValidationException("The 'Name' supports a maximum of 15 characters.");

            _text = text;
        }

        public override string ToString() => _text;


    }


}
