﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.VDC.ValueObjects
{
    public sealed class MultiSelectionValidationException : DomainException
    {
        public MultiSelectionValidationException(string message) : base(message)
        {
        }
    }
}
