﻿using System;

namespace Domain.VDC.ValueObjects
{
    [Serializable]
    public sealed class IdentifierValidationException : DomainException
    {

        public IdentifierValidationException(string message) : base(message)
        {
        }


    }
}