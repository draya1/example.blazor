﻿namespace Domain.VDC.ValueObjects
{
    public readonly struct ShortName
    {

        private readonly string _text;

        public ShortName(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) throw new NameValidationException("The 'Name' is required.");

            if (text.Length > 25) throw new NameValidationException("The 'Name' supports a maximum of 25 characters.");

            _text = text;
        }

        public override string ToString() => _text;


    }
}
