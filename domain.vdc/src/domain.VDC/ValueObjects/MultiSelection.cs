﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.VDC.ValueObjects
{
    public struct MultiSelection
    {
        private string _value { get; }
        private List<int> _valueList { get; } 

        public MultiSelection(string ids)
        {
            _value = string.IsNullOrWhiteSpace(ids) ? string.Empty : ids;
            _valueList = new List<int>();

            if (_value != null)
            {
                foreach (var item in _value.Split(";"))
                {
                    if (!string.IsNullOrWhiteSpace(item) && !int.TryParse(item, out int value)) throw new MultiSelectionValidationException("The 'MultiSelection' value must be an integer.");
                }

                _valueList = _value.Split(";").Where(s => int.TryParse(s, out int value)).Select(s => int.Parse(s)).ToList();
            }

        }

        public MultiSelection(int[] ids)
            :this(ids !=null && ids.Length > 0 ? ids.ToList().Select(s => s.ToString()).Aggregate((x, y) => x + ";" + y) : null)
        {            
        }

        public List<int> ToList() => _valueList ?? new List<int>();

        public int[] ToArray() => _valueList != null ? _valueList.ToArray(): new int[] { };

        public override string ToString() => _value;

    }
}
