﻿namespace Domain.VDC.ValueObjects
{
    public struct LongName
    {
        private readonly string _text;

        public LongName(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) throw new NameValidationException("The 'Name' is required.");

            if (text.Length > 100) throw new NameValidationException("The 'Name' supports a maximum of 100 characters.");

            _text = text;
        }

        public override string ToString() => _text;
    }
}
