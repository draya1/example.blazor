﻿using System.Threading.Tasks;

namespace Domain.VDC
{
    public interface IUnitOfWork
    {
        Task<int> Save();
    }
}
