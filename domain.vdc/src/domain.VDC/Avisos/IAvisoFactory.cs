﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.VDC.Avisos
{
    public interface IAvisoFactory
    {
        IAviso NewAviso(int? tipoAviso, int? categoriaAviso,
            string titulo, string texto,
            DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta,
            bool? activo, List<AvisoUsuario> avisosUsuarios);
    }

    public interface IAviso : IAggregateRoot
    {        
        void Activar(bool? activo);
        void LeidoBy(string username);
        void AddUser(string userName);
        void RemoveUser(string userName);
    }
    public interface IAvisoRepository
    {
        Task<IAviso> GetBy(int id);
        Task<IAviso> GetWithUsersBy(int id);
        Task Add(IAviso item);
        Task Update(IAviso item);
    }

    public interface IAvisoPersistenceValidator
    {
        void Validate(IAviso entity);
    }

    public interface IAvisoQuerys
    {
        IQueryable<AvisoDTO> GetCabecerasAvisosFiltered(string search = "", StringComparison stringComparer = StringComparison.OrdinalIgnoreCase);
        IQueryable<Aviso> GetAvisos();
        IQueryable<AvisoDTO> GetCabecerasAvisos(bool? activo = null, bool? leidos = null, int? tipo = null, string usuario = "", DateTime? fDesde = null, DateTime? fHasta = null);
        IQueryable<AvisoTipo> GetTiposAvisos();
        IQueryable<AvisoCategoria> GetCategoriasAvisos();        
    }
    public interface IAvisoService
    {
        IAviso New(int? tipoAviso, int? categoriaAviso, 
            string titulo, string texto, 
            DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, 
            bool? activo, List<AvisoUsuario> avisosUsuarios);
        IAviso New(IAviso item);
        IAviso Set(IAviso item, bool? activo=null, string leidoByUserName=null);
    }

}
