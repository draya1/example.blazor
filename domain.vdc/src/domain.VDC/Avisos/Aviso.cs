﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.VDC.Avisos
{    
    public class Aviso : IAviso
    {
        public Aviso()
        {            
            AvisosUsuarios = new List<AvisoUsuario>();
        }
        public Aviso(int id, int? avisoTipoId, int? avisoCategoriaId, DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, string titulo, string texto, bool? activo) : this()
        {
            Id = id;
            FechaCreacion = fechaCreacion;
            FechaDesde = fechaDesde;
            FechaHasta = fechaHasta;
            AvisoTipoId = avisoTipoId;            
            AvisoCategoriaId = avisoCategoriaId;            
            Titulo = titulo;
            Texto = texto;            
            Activo = activo;            
        }

        public Aviso(int? avisoTipoId, int? avisoCategoriaId, DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, string titulo, string texto, bool? activo, List<AvisoUsuario> avisosUsuarios)
            :this(0, avisoTipoId, avisoCategoriaId, fechaCreacion, fechaDesde, fechaHasta, titulo, texto, activo)
        {
            AvisosUsuarios = avisosUsuarios;
        }

        public Aviso(int? avisoTipoId, int? avisoCategoriaId, DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, string titulo, string texto, bool? activo)
            : this(avisoTipoId, avisoCategoriaId, fechaCreacion, fechaDesde, fechaHasta, titulo, texto, activo, new List<AvisoUsuario>())
        {            
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public int? AvisoTipoId { get; set; }
        public int? AvisoCategoriaId { get; set; }        
        public string Texto { get; set; }
        public bool? Activo { get; set; }
        public virtual List<AvisoUsuario> AvisosUsuarios { get; set; }

        public void AddUser(string userName)
        {
            if (!this.AvisosUsuarios.Any(p => p.UserName == userName))
                this.AvisosUsuarios.Add(new AvisoUsuario(0, this.Id, userName, null));

        }

        public void RemoveUser(string userName)
        {
            if (this.AvisosUsuarios.Any(p => p.UserName == userName))
                this.AvisosUsuarios.RemoveAll(p => p.UserName == userName);
        }

        public void LeidoBy(string username)
        {
            var data = this.AvisosUsuarios.FirstOrDefault(p => p.UserName == username);
            if (data != null) data.FechaLeido = DateTime.Now;
        }

        public void Activar(bool? activo)
        {
            this.Activo = activo;
        }
    }

    public class AvisoCategoria
    {
        public AvisoCategoria()
        {
        }

        public AvisoCategoria(int id, string descripcion)
        {
            Id = id;
            Descripcion = descripcion;
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
    }

    public class AvisoTipo
    {
        public AvisoTipo()
        {
        }

        public AvisoTipo(int id, string descripcion)
        {
            Id = id;
            Descripcion = descripcion;
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
    }

    public class AvisoUsuario
    {
        public AvisoUsuario()
        {
        }

        public AvisoUsuario(int id, int? avisoId, string userName, DateTime? fechaLeido)
        {
            Id = id;
            AvisoId = avisoId;
            UserName = userName;
            FechaLeido = fechaLeido;
        }

        public int Id { get; set; }
        public int? AvisoId { get; set; }
        public string UserName { get; set; }
        public DateTime? FechaLeido { get; set; }
    }
}
