﻿using System;

namespace Domain.VDC.Avisos
{
    public class AvisoDTO
    {
        public int Id { get; set; }
        public string Titulo { get; set; }        
        public int? TipoId { get; set; }
        public string Tipo { get; set; }
        public int? CategoriaId { get; set; }
        public string Categoria { get; set; }
        public string Texto { get; set; }
        public bool? Activo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }        
        public DateTime? FechaLeido { get; set; }
        public string UserName { get; set; }

        public string StringId { get; set; }
    }
}
