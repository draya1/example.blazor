﻿using System.Linq;

namespace Domain.VDC.Usuarios
{
    public interface IUsuarioFactory
    {

    }

    public interface IUsuarioQuerys
    {
        IQueryable<Usuario> GetUsuarios();
        IQueryable<Rol> GetRoles();
        IQueryable<UsuarioExterno> GetUsuariosExternos();
    }
}
