﻿namespace Domain.VDC.Usuarios
{
    public class Usuario
    {
        public Usuario()
        {
        }

        public Usuario(string id, string rolId, string userName, string rolName, int externalUserId, int medicoId)
        {
            Id = id;
            RolId = rolId;
            UserName = userName;
            RolName = rolName;
            ExternalUserId = externalUserId;
            MedicoId = medicoId;
        }

        public string Id { get; set;  }
        public string RolId { get; set;  }
        public string UserName { get; set; }
        public string RolName { get; set; }
        public int? ExternalUserId { get; set;  }
        public int? MedicoId { get; set;  }
        
    }

    public class UsuarioExterno
    {
        public UsuarioExterno()
        {
        }

        public UsuarioExterno(int id, int? medicoId, string nombre, string abreviacion)
        {
            Id = id;
            MedicoId = medicoId;
            Nombre = nombre;
            Abreviacion = abreviacion;
        }

        public int Id { get; set; }
        public int? MedicoId { get; set; }
        public string Nombre { get; set; }
        public string Abreviacion { get; set; }

    }
}
