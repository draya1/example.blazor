﻿namespace Domain.VDC.Usuarios
{
    public class Rol
    {
        public Rol()
        {
        }

        public Rol(string id, string rolName)
        {
            Id = id;
            RolName = rolName;
        }

        public string Id { get; set;  }
        public string RolName { get; set; }
    }
}
