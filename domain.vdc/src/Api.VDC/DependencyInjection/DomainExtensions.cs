﻿using Domain.VDC.Avisos;
using Infrastructure.VDC.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace Api.VDC.DependencyInjection
{
    public static class DomainExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<IAvisoPersistenceValidator, AvisoPersistenceValidator>();
            services.AddScoped<IAvisoService, AvisoService>();

            return services;
        }
    }
}
