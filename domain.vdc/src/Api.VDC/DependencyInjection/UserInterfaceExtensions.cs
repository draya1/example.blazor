﻿using Api.VDC.UseCases.Avisos;
using Application.VDC.UseCases.AvisoUC;
using Microsoft.Extensions.DependencyInjection;

namespace Api.VDC.DependencyInjection
{
    public static class UserInterfaceExtensions
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            

            //Avisos
            services.AddScoped<AvisoPresenter, AvisoPresenter>();
            services.AddScoped<IAvisoSucessOutputPort>(x => x.GetRequiredService<AvisoPresenter>());
            

            return services;
        }
    }
}
