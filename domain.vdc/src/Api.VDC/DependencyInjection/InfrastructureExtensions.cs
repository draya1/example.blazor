﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Domain.VDC.Usuarios;
using Infrastructure.VDC.Configuration;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Querys;
using Infrastructure.VDC.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api.VDC.DependencyInjection
{
    public static class InfrastructureExtensions
    {
        public static IServiceCollection AddSQLServerPersistence(this IServiceCollection services, IConfiguration configuration)
        {   
            services.AddDbContext<CedContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection").Replace("<DbPassword>",configuration["DbPassword"]) ));

            AddCommonServices(services);

            return services;

        }

        public static IServiceCollection AddSQLServerInMemory(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddDbContext<CedContext>(
                options => options.UseInMemoryDatabase(databaseName: "CED").ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                );

            AddCommonServices(services);

            return services;
        }

        private static void AddCommonServices(IServiceCollection services)
        {

            //factory            
            services.AddScoped<IAvisoFactory, EntityFactory>();
            

            //Data
            services.AddScoped<ISeedDataService, SeedDataService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //Repositorios
            
            services.AddScoped<IAvisoRepository, AvisoRepository>();
            

            //Querys            
            services.AddScoped<IUsuarioQuerys, UsuarioQuerys>();
            services.AddScoped<IAvisoQuerys, AvisoQuerys>();

        }
    }
}
