﻿using Domain.VDC.Usuarios;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.VDC.UseCases.Usuarios.List
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuarioQuerys _usuarioQuerys;

        public UsuariosController(IUsuarioQuerys usuarioQuerys)
        {
            _usuarioQuerys = usuarioQuerys;
        }

        /// <summary>
        /// Obtiene lista usuarios.
        /// </summary>
        /// <response code="200">Lista de usuarios obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de usuarios. </returns>
        [HttpGet]
        public IEnumerable<Usuario> Get()
        {
            return _usuarioQuerys.GetUsuarios().ToList();
        }

        /// <summary>
        /// Obtiene lista usuarios externos.
        /// </summary>
        /// <response code="200">Lista de usuarios externos obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de usuarios externos. </returns>
        [Route("[action]")]
        [HttpGet]
        public IEnumerable<UsuarioExterno> GetExternos()
        {
            return _usuarioQuerys.GetUsuariosExternos().ToList();
        }

        /// <summary>
        /// Obtiene lista roles.
        /// </summary>
        /// <response code="200">Lista de roles obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de roles. </returns>
        [Route("[action]")]
        [HttpGet]
        public IEnumerable<Rol> GetRoles()
        {
            return _usuarioQuerys.GetRoles().ToList();
        }

    }
}
