﻿using Application.VDC.UseCases.AvisoUC;
using Domain.VDC.Avisos;
using FluentMediator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Api.VDC.UseCases.Avisos.Create
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvisosController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly AvisoPresenter _presenter;

        public AvisosController(IMediator mediator, AvisoPresenter presenter)
        {
            _mediator = mediator;
            _presenter = presenter;
        }

        /// <summary>
        /// Crea aviso.
        /// </summary>
        /// <response code="200">Aviso creado correctamente.</response>        
        /// <response code="400">Petición incorrecto.</response>        
        /// <response code="500">Error.</response>
        /// <param name="request">Petición para crear un aviso.</param>
        /// <returns>OkResult. </returns>        
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody][Required] CreateAvisoRequest request)
        {

            var aviso = new Aviso(request.AvisoTipoId, request.AvisoCategoriaId, request.FechaCreacion, request.FechaDesde, request.FechaHasta, request.Titulo, request.Texto, request.Activo);
            aviso.AvisosUsuarios = request.AvisosUsuarios;

            var input = new CreateAvisoInput(aviso);
            await _mediator.PublishAsync(input);
            return _presenter.ViewModel;

        }
    }

    
}
