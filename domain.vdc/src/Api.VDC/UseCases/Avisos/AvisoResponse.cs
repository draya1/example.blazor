﻿using Domain.VDC.Avisos;
using System;
using System.Collections.Generic;

namespace Api.VDC.UseCases.Avisos
{
    public class AvisoResponse
    {
        public AvisoResponse(int id, string titulo, 
            DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, 
            int? avisoTipoId, int? avisoCategoriaId, 
            string texto, bool? activo, List<AvisoUsuario> avisosUsuarios)
        {
            Id = id;
            Titulo = titulo;
            FechaCreacion = fechaCreacion;
            FechaDesde = fechaDesde;
            FechaHasta = fechaHasta;
            AvisoTipoId = avisoTipoId;
            AvisoCategoriaId = avisoCategoriaId;
            Texto = texto;
            Activo = activo.GetValueOrDefault(false);
            AvisosUsuarios = avisosUsuarios;
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public int? AvisoTipoId { get; set; }
        public int? AvisoCategoriaId { get; set; }
        public string Texto { get; set; }
        public bool Activo { get; set; }
        public List<AvisoUsuario> AvisosUsuarios { get; set; }
    }
}
