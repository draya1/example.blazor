﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Api.VDC.UseCases.Avisos.Get
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvisosController : ControllerBase
    {
        private readonly IAvisoQuerys _avisoQuerys;

        public AvisosController(IAvisoQuerys avisoQuerys)
        {
            _avisoQuerys = avisoQuerys;
        }

        /// <summary>
        /// Obtiene aviso.
        /// </summary>
        /// <response code="200">Aviso obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="404">Aviso no encontrado.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Aviso.</returns>        
        [Route("[action]/{id}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AvisoResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public Task<IActionResult> GetAviso(int id)
        {
            var result = _avisoQuerys.GetAvisos().FirstOrDefault(p => p.Id == id);
            if (result == null) throw new NotFoundException("No existe aviso.");

            IActionResult response = 
                new OkObjectResult(
                    new AvisoResponse(result.Id,result.Titulo,
                    result.FechaCreacion,result.FechaDesde, result.FechaHasta, 
                    result.AvisoTipoId, result.AvisoCategoriaId, 
                    result.Texto, result.Activo, result.AvisosUsuarios
                    ));
            
            return Task.FromResult( response );
            
        }
    }
}
