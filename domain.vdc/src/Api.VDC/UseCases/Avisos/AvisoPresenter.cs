﻿using Application.VDC.UseCases.AvisoUC;
using Microsoft.AspNetCore.Mvc;

namespace Api.VDC.UseCases.Avisos
{
    public class AvisoPresenter : IAvisoSucessOutputPort
    {
        public IActionResult ViewModel { get; private set; }
        public void NotFound(string message)
        {
            ViewModel = new NotFoundObjectResult(new { error = message });
        }

        public void Standard(AvisoSuccessOutput output)
        {
            ViewModel = new OkObjectResult(output);
        }
    }
}
