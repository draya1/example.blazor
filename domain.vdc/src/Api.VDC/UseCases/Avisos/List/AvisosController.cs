﻿using Domain.VDC.Avisos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.VDC.UseCases.Avisos.List
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AvisosController : ControllerBase
    {
        private readonly IAvisoQuerys _avisoQuerys;

        public AvisosController(IAvisoQuerys avisoQuerys)
        {
            _avisoQuerys = avisoQuerys;
        }


        /// <summary>
        /// Obtiene lista de cabeceras avisos.
        /// </summary>
        /// <response code="200">Lista de cabeceras avisos obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de cabeceras avisos.</returns>                
        [HttpGet()]
        public object GetAvisos()
        {
            IQueryable<AvisoDTO> data;

            var queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                StringValues Skip;
                StringValues Take;
                StringValues Search;
                StringValues Order;
                StringComparison sc = StringComparison.OrdinalIgnoreCase;
                int skip = queryString.TryGetValue("$skip", out Skip) ? Convert.ToInt32(Skip[0]) : 0;
                int top = queryString.TryGetValue("$top", out Take) ? Convert.ToInt32(Take[0]) : 10;
                string search = queryString.TryGetValue("$search", out Search) ? Search[0].ToString() : string.Empty;
                string order = queryString.TryGetValue("$order", out Order) ? Order[0].ToString() : string.Empty;

                data = _avisoQuerys.GetCabecerasAvisosFiltered(search, sc);

                if (!string.IsNullOrWhiteSpace(order))
                {
                    data = OrderData(order, data);
                }

                return new { Items = data.Skip(skip).Take(top), Count = data.Count() };

            }
            else
            {
                data = _avisoQuerys.GetCabecerasAvisosFiltered();
                return data;
            }

        }
        private IQueryable<AvisoDTO> OrderData(string orderColumn, IQueryable<AvisoDTO> data) =>
           orderColumn.ToLower() switch
           {

               "-id" => data.OrderByDescending(o => o.Id),
               "-titulo" => data.OrderByDescending(o => o.Titulo),
               "-categoria" => data.OrderByDescending(o => o.Categoria),
               "-tipo" => data.OrderByDescending(o => o.Tipo),
               "-fechacreacion" => data.OrderByDescending(o => o.FechaCreacion),
               "-fechadesde" => data.OrderByDescending(o => o.FechaDesde),
               "-fechahasta" => data.OrderByDescending(o => o.FechaHasta),
               "-activo" => data.OrderByDescending(o => o.Activo),
               "titulo" => data.OrderBy(o => o.Titulo),
               "categoria" => data.OrderBy(o => o.Categoria),
               "tipo" => data.OrderBy(o => o.Tipo),
               "fechacreacion" => data.OrderBy(o => o.FechaCreacion),
               "fechadesde" => data.OrderBy(o => o.FechaDesde),
               "fechahasta" => data.OrderBy(o => o.FechaHasta),
               "activo" => data.OrderBy(o => o.Activo),
               _ => data.OrderBy(o => o.Id)
           };


        /// <summary>
        /// Obtiene lista de avisos.
        /// </summary>
        /// <response code="200">Lista de avisos obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de avisos.</returns>        
        [Route("[action]")]
        [HttpGet("AvisosDetallados")]
        public object GetAvisosDetallados()
        {
            return _avisoQuerys.GetAvisos().ToList();
        }

        /// <summary>
        /// Obtiene lista de avisos usuarios.
        /// </summary>
        /// <response code="200">Lista de avisos usuarios obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de avisos usuarios. </returns>
        [Route("[action]")]
        [HttpGet("UsuariosAvisos")]
        public IEnumerable<AvisoDTO> GetUsuariosAvisos(string user, bool sesion, DateTime? fechaActual = null)
        {

            List<AvisoDTO> result = new List<AvisoDTO>();
            
            DateTime? fDesde = fechaActual.HasValue ? fechaActual : DateTime.Now;
            DateTime? fHasta = fechaActual.HasValue ? fechaActual: DateTime.Now;
                        
            if (sesion)
            {
                result.AddRange(_avisoQuerys.GetCabecerasAvisos(true, false, 3, user, fDesde, fHasta).ToList());
                result.AddRange(_avisoQuerys.GetCabecerasAvisos(true, null, 4, user, fDesde, fHasta).ToList());
            }
            else
            {
                result.AddRange(_avisoQuerys.GetCabecerasAvisos(true, false, 1, user, fDesde, fHasta).ToList());
                result.AddRange(_avisoQuerys.GetCabecerasAvisos(true, false, 2, user, fDesde, fHasta).ToList());
            }

            return result;
        }

        /// <summary>
        /// Obtiene lista de tipos de aviso.
        /// </summary>
        /// <response code="200">Lista de tipos de aviso obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de tipos de aviso. </returns>
        [Route("[action]")]
        [HttpGet("TiposAvisos")]
        public IEnumerable<AvisoTipo> GetTiposAvisos()
        {  
           return _avisoQuerys.GetTiposAvisos().ToList();
        }

        /// <summary>
        /// Obtiene lista de categorias de aviso.
        /// </summary>
        /// <response code="200">Lista de categorias de aviso obtenida correctamente.</response>
        /// <response code="400">Petición incorrecta.</response>        
        /// <response code="500">Error.</response>        
        /// <returns>Lista de categorias de aviso. </returns>
        [Route("[action]")]
        [HttpGet("CategoriasAvisos")]
        public IEnumerable<AvisoCategoria> GetCategoriasAvisos()
        {
            return _avisoQuerys.GetCategoriasAvisos().ToList();
        }
    }
}
