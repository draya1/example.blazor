using Domain.VDC;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.VDC.Filters
{
    public sealed class BusinessExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {

            
            if (context.Exception is DomainException)
            {
                var problemDetails = new ProblemDetails
                {
                    Status = 400,
                    Title = "Bad Request",
                    Detail = context.Exception.Message
                };

                

                if (context.Exception is DuplicateNameException)                
                    problemDetails.Detail = $"El nombre {context.Exception.Message} ya existe.";                     

                if (context.Exception is WithReferencesException)
                    problemDetails.Detail = "No se puede desactivar: La agrupación tiene referencias.";
                

                if (context.Exception is DuplicateReferenceException)
                    problemDetails.Detail = $"El elemento {context.Exception.Message} ya está asignado.";



                if (context.Exception is NotFoundException)
                {
                    problemDetails.Status = 404;
                    problemDetails.Title = "Not Found";
                    context.Result = new NotFoundObjectResult(problemDetails);
                }
                else
                {

                    context.Result = new BadRequestObjectResult(problemDetails);                 
                }

                context.Exception = null;


            }
        }
    }
}