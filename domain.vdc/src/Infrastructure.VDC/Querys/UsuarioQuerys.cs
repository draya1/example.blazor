﻿using Domain.VDC.Usuarios;
using Infrastructure.VDC.Data;
using System.Linq;

namespace Infrastructure.VDC.Querys
{
    public class UsuarioQuerys : IUsuarioQuerys
    {
        private readonly CedContext _context;

        public UsuarioQuerys(CedContext context)
        {
            _context = context;
        }

        public IQueryable<Rol> GetRoles()
        {
            return _context.Roles;
        }

        public IQueryable<Usuario> GetUsuarios()
        {
            return _context.Usuarios;            
        }
        public IQueryable<UsuarioExterno> GetUsuariosExternos()
        {
            return _context.UsuariosExternos;
        }
    }
}
