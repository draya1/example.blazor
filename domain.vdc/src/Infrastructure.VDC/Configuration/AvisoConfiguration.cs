﻿using Domain.VDC.Avisos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.VDC.Configuration
{
    public class AvisoConfiguration : IEntityTypeConfiguration<Aviso>
    {
        public void Configure(EntityTypeBuilder<Aviso> builder)
        {
            builder.ToTable("Avisos");            
            builder.HasOne<AvisoTipo>().WithMany().HasForeignKey(fk => fk.AvisoTipoId).OnDelete(DeleteBehavior.SetNull);
            builder.HasOne<AvisoCategoria>().WithMany().HasForeignKey(fk => fk.AvisoCategoriaId).OnDelete(DeleteBehavior.SetNull);            
            
            builder.HasKey(k => k.Id);            
        }
    }

    public class AvisoTipoConfiguration : IEntityTypeConfiguration<AvisoTipo>
    {
        public void Configure(EntityTypeBuilder<AvisoTipo> builder)
        {
            builder.ToTable("AvisosTipo");
            builder.HasKey(k => k.Id);
        }
    }

    public class AvisoImportanciaConfiguration : IEntityTypeConfiguration<AvisoCategoria>
    {
        public void Configure(EntityTypeBuilder<AvisoCategoria> builder)
        {
            builder.ToTable("AvisosCategoria");
            builder.HasKey(k => k.Id);
        }
    }

    public class AvisoUsuarioConfiguration : IEntityTypeConfiguration<AvisoUsuario>
    {
        public void Configure(EntityTypeBuilder<AvisoUsuario> builder)
        {
            builder.ToTable("AvisosUsuarios");
            builder.HasOne<Aviso>().WithMany(p=>p.AvisosUsuarios).HasForeignKey(fk => fk.AvisoId).OnDelete(DeleteBehavior.Cascade);
            builder.HasKey(k => k.Id);
        }
    }
}
