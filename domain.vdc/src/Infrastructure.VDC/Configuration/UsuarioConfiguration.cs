﻿using Domain.VDC.Usuarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.VDC.Configuration
{
    public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToView("ListUsuarios");
            builder.HasKey(k => new { k.Id, k.RolId });
        }
        
    }

    public class UsuarioExternoConfiguration : IEntityTypeConfiguration<UsuarioExterno>
    {
        public void Configure(EntityTypeBuilder<UsuarioExterno> builder)
        {
            builder.ToView("ListUsuariosExternos");
            builder.HasKey(k => new { k.Id });
        }

    }
}
