﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.VDC.Configuration
{
    public static class SeedData
    {
        public static void Seed(ModelBuilder builder)
        {


            //AVISOS
            builder.Entity<Domain.VDC.Avisos.Aviso>().HasData(
                new Domain.VDC.Avisos.Aviso(1, 1, 1, new System.DateTime(2020, 1, 1, 1, 11, 11), new System.DateTime(2020, 1, 1, 1, 11, 11), new System.DateTime(2020, 1, 1, 1, 12, 11), "Aviso 01", "Aviso 01 - inmediato", true),
                new Domain.VDC.Avisos.Aviso(2, 2, 2, new System.DateTime(2020, 2, 2, 2, 22, 22), new System.DateTime(2020, 2, 15, 14, 00, 00), new System.DateTime(2020, 2, 15, 14, 00, 00), "Aviso 01", "Aviso 02 - programado", true),
                new Domain.VDC.Avisos.Aviso(3, 3, 3, new System.DateTime(2020, 2, 2, 2, 22, 22), new System.DateTime(2020, 2, 15, 14, 00, 00), new System.DateTime(2020, 2, 25, 20, 00, 00), "Aviso 03", "Aviso 03 - inicio sesión", true),
                new Domain.VDC.Avisos.Aviso(4, 4, 4, new System.DateTime(2020, 2, 2, 2, 22, 22), new System.DateTime(2020, 2, 15, 14, 00, 00), new System.DateTime(2020, 2, 25, 20, 00, 00), "Aviso 04", "Aviso 04 - inicio sesión repetido", true),
                new Domain.VDC.Avisos.Aviso(5, 1, 1, new System.DateTime(2019, 1, 1, 1, 11, 11), null, null, "Aviso 05", "Aviso 05 - inmediato", false)
                );

            builder.Entity<Domain.VDC.Avisos.AvisoTipo>().HasData(
                new Domain.VDC.Avisos.AvisoTipo(1, "Inmediato"),
                new Domain.VDC.Avisos.AvisoTipo(2, "Programado"),
                new Domain.VDC.Avisos.AvisoTipo(3, "Inicio Sesión"),
                new Domain.VDC.Avisos.AvisoTipo(4, "Inicio Sesión Repetición")
                );

            builder.Entity<Domain.VDC.Avisos.AvisoCategoria>().HasData(
                new Domain.VDC.Avisos.AvisoCategoria(1, "Informativo"),
                new Domain.VDC.Avisos.AvisoCategoria(2, "Alerta"),
                new Domain.VDC.Avisos.AvisoCategoria(3, "Urgente")
                );

            builder.Entity<Domain.VDC.Avisos.AvisoUsuario>().HasData(

                new Domain.VDC.Avisos.AvisoUsuario(1, 1, "alice", null),
                new Domain.VDC.Avisos.AvisoUsuario(2, 1, "bob", null),
                new Domain.VDC.Avisos.AvisoUsuario(3, 2, "alice", null),
                new Domain.VDC.Avisos.AvisoUsuario(4, 3, "bob", null),
                new Domain.VDC.Avisos.AvisoUsuario(5, 4, "bob", new System.DateTime(2019, 1, 1, 1, 11, 15)),
                new Domain.VDC.Avisos.AvisoUsuario(6, 5, "alice", new System.DateTime(2019, 1, 1, 1, 11, 18)),
                new Domain.VDC.Avisos.AvisoUsuario(7, 5, "bob", null)
                );


            //USUARIOS
            builder.Entity<Domain.VDC.Usuarios.Usuario>().HasData(
                new Domain.VDC.Usuarios.Usuario("u0", "R1", "master", "admin", 0, 0),
                new Domain.VDC.Usuarios.Usuario("u1", "R1", "alice", "admin", 1, 1),
                new Domain.VDC.Usuarios.Usuario("u1", "R2", "alice", "medico", 1, 1),
                new Domain.VDC.Usuarios.Usuario("u2", "R2", "bob", "medico", 3, 2),
                new Domain.VDC.Usuarios.Usuario("u1", "R3", "alice", "visionmedico", 1, 1)
                );

            //USUARIOS EXTERNOS
            builder.Entity<Domain.VDC.Usuarios.UsuarioExterno>().HasData(
                new Domain.VDC.Usuarios.UsuarioExterno(1, 1, "Alice Smith", "ALS"),
                new Domain.VDC.Usuarios.UsuarioExterno(3, 2, "Bob Smith", "BBS")
                );

            //ROLES
            builder.Entity<Domain.VDC.Usuarios.Rol>().HasData(
                new Domain.VDC.Usuarios.Rol("R1", "admin"),
                new Domain.VDC.Usuarios.Rol("R2", "medico"),
                new Domain.VDC.Usuarios.Rol("R3", "visionmedico")
                );


        }
    }
}
