﻿using Domain.VDC;
using Infrastructure.VDC.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.VDC.Repositories
{

    public class BaseRepository<TModelEntity, IDomainEntity>
    where TModelEntity : class, IDomainEntity, new()
    where IDomainEntity : IEntity
    {
        protected readonly CedContext _context;
        protected readonly string _itemName;

        public BaseRepository(CedContext context, string itemName)
        {
            _context = context;
            _itemName = itemName;
        }

        public virtual async Task<IDomainEntity> GetBy(int id)
        {
            if (id == -1) return GetDefault();

            IDomainEntity result = await _context.Set<TModelEntity>().Where(x => x.Id == id).SingleOrDefaultAsync();

            if (result is null)
            {
                throw new NotFoundException($"The {_itemName} {id} does not exist or is not processed yet.");
            }

            return result;
        }
        public virtual async Task Add(IDomainEntity item)
        {
            _context.Set<TModelEntity>().Add((TModelEntity)item);
            await _context.SaveChangesAsync();
        }
        public virtual async Task Update(IDomainEntity item)
        {
            _context.Set<TModelEntity>().Update((TModelEntity)item);
            await _context.SaveChangesAsync();
        }

        protected virtual IDomainEntity GetDefault()
        {
            return new TModelEntity();
        }
    }


}