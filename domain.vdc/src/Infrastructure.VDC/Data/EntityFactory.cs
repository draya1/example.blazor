﻿using Domain.VDC.Avisos;
using System;
using System.Collections.Generic;

namespace Infrastructure.VDC.Data
{
    public sealed class EntityFactory : IAvisoFactory
        
    {
       
        public IAviso NewAviso(int? tipoAviso, int? categoriaAviso, string titulo, string texto, DateTime? fechaCreacion, DateTime? fechaDesde, DateTime? fechaHasta, bool? activo, List<AvisoUsuario> avisosUsuarios)
        {
            return new Aviso(tipoAviso, categoriaAviso, fechaCreacion, fechaDesde, fechaHasta, titulo, texto, activo, avisosUsuarios);
        }

        
        
    }
}
