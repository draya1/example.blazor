﻿using Infrastructure.VDC.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.VDC.Data
{
    public class CedContext : DbContext
    {
        public CedContext() { }

        public CedContext(DbContextOptions<CedContext> options) : base(options)
        {
        }

        
        public DbSet<Domain.VDC.Avisos.AvisoDTO> QAvisos { get; set; } = null;
        public DbSet<Domain.VDC.Avisos.Aviso> Avisos { get; set; } = null;
        public DbSet<Domain.VDC.Avisos.AvisoTipo> AvisosTipo { get; set; } = null;
        public DbSet<Domain.VDC.Avisos.AvisoCategoria> AvisosCategoria { get; set; } = null;
        public DbSet<Domain.VDC.Avisos.AvisoUsuario> AvisosUsuario { get; set; } = null;
        public DbSet<Domain.VDC.Usuarios.Usuario> Usuarios { get; set; } = null;
        public DbSet<Domain.VDC.Usuarios.Rol> Roles { get; set; } = null;
        public DbSet<Domain.VDC.Usuarios.UsuarioExterno> UsuariosExternos { get; set; } = null;



        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(CedContext).Assembly);
            SeedData.Seed(builder);
        }


    }
}
