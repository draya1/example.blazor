﻿using Domain.VDC;
using System;
using System.Threading.Tasks;

namespace Infrastructure.VDC.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        private readonly CedContext _context;
        private bool _disponsed = false;

        public UnitOfWork(CedContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disponsing)
        {
            if (!_disponsed)
            {
                if (disponsing)
                {
                    _context.Dispose();
                }
                _disponsed = true;
            }
        }

        public async Task<int> Save()
        {
            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows;
        }
    }
}
