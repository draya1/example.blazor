﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using System;
using System.Linq;

namespace Infrastructure.VDC.Validators
{
    public class AvisoPersistenceValidator : BaseValidator<Aviso, IAviso>, IAvisoPersistenceValidator
    {
        public AvisoPersistenceValidator(CedContext context) : base(context, "aviso")
        {
        }

        public override void Validate(IAviso entity)
        {
            base.Validate(entity);


            var item = (Aviso)entity;

            if (string.IsNullOrEmpty(item.Titulo)) throw new DomainException("Título debe tener valor.");
            if (string.IsNullOrEmpty(item.Texto)) throw new DomainException("Texto debe tener valor.");

            if (item.AvisoTipoId.HasValue == false) throw new DomainException("Tipo debe tener valor.");
            if (item.AvisoCategoriaId.HasValue == false) throw new DomainException("Categoria debe tener valor.");

            if (item.FechaCreacion.HasValue == false) throw new DomainException("Fecha Creación debe tener valor.");
            if (item.FechaDesde.HasValue == false) throw new DomainException("Fecha Desde debe tener valor.");
            if (item.FechaHasta.HasValue == false) throw new DomainException("Fecha Hasta debe tener valor.");
                                    
            if (item.FechaHasta < DateTime.Now) throw new DomainException("Fecha Hasta no puede ser menor a la actual.");
            if (item.FechaHasta <= item.FechaDesde) throw new DomainException("Fecha Hasta no puede ser inferior a Fecha Desde.");

            if (item.AvisosUsuarios == null || item.AvisosUsuarios.Any() == false) throw new DomainException("Debe indicarse al menos un destinatario.");


            //references
            if (_context.AvisosTipo.Find(item.AvisoTipoId) is null) throw new ReferenceException($"'AvisoTipoId: {item.AvisoTipoId}'");
            if (_context.AvisosCategoria.Find(item.AvisoCategoriaId) is null) throw new ReferenceException($"'AvisoCategoriaId: {item.AvisoCategoriaId}'");


        }
    }
}

