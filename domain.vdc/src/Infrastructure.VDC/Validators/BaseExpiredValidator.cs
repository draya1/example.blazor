using Domain.VDC;
using Infrastructure.VDC.Data;
using System.Linq;

namespace Infrastructure.VDC.Validators
{
    public class BaseValidator <TModelEntity, IDomainEntity>
        where TModelEntity :  class, IDomainEntity, new ()
        where IDomainEntity: IEntity
    {
        protected readonly CedContext _context;
        protected readonly string _itemName;

        public BaseValidator(CedContext context, string itemName)
        {
            _context = context;
            _itemName = itemName;
        }

        public virtual void Validate(IDomainEntity entity)
        {
            //not found
            if (entity.Id != 0 && _context.Set<TModelEntity>().Find(entity.Id) == null) throw new NotFoundException($"The {_itemName} {entity.Id} does not exist or is not processed yet.");            
        }
    }    
    
}