﻿using System.Threading.Tasks;

namespace Application.VDC.UseCases
{
    public interface IUseCase<in TUseCaseInput> where TUseCaseInput : IUseCaseInput
    {
        Task Execute(TUseCaseInput input);
    }
}
