﻿namespace Application.VDC.UseCases.AvisoUC
{

    public interface IUpdateLeidoAvisoUseCase : IUseCase<UpdateLeidoAvisoInput> { }

    public interface IAvisoSucessOutputPort : IOutputPortStandard<AvisoSuccessOutput>, IOutputPortNotFound  { }

    public class UpdateLeidoAvisoInput : IUseCaseInput
    {
        public UpdateLeidoAvisoInput(int avisoId, string userName)
        {
            AvisoId = avisoId;
            UserName = userName;
        }

        public int AvisoId { get; }
        public string UserName { get; }
    }
    
}
