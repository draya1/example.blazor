﻿using Domain.VDC.Avisos;

namespace Application.VDC.UseCases.AvisoUC
{
    public interface ICreateAvisoUseCase : IUseCase<CreateAvisoInput> { }
    public interface ICreateAvisoOutputPort : IOutputPortStandard<AvisoSuccessOutput> { }
    public class CreateAvisoInput : IUseCaseInput
    {
        public CreateAvisoInput(Aviso aviso)
        {
            Aviso = aviso;
        }

        public Aviso Aviso { get; }
            
    }
}
