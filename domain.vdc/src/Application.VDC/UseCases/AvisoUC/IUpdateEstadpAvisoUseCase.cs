﻿namespace Application.VDC.UseCases.AvisoUC
{
    public interface IUpdateEstadoAvisoUseCase : IUseCase<UpdateEstadoAvisoInput> { }

    public class UpdateEstadoAvisoInput : IUseCaseInput
    {
        public UpdateEstadoAvisoInput(int avisoId, bool activo)
        {
            AvisoId = avisoId;
            Activo = activo;
        }

        public int AvisoId { get; }
        public bool Activo { get; }
    }
}
