﻿using Domain.VDC.Avisos;
using System.Threading.Tasks;

namespace Application.VDC.UseCases.AvisoUC.Adaptadores
{
    public class UpdateLeidoAvisoUseCase : CrudAviso, IUpdateLeidoAvisoUseCase
    {
        public UpdateLeidoAvisoUseCase(IAvisoSucessOutputPort output, IAvisoRepository repository, IAvisoService service) : base(output, repository, service)
        {
        }

        public Task Execute(UpdateLeidoAvisoInput input)
        {
            return UpdateLeido(input.AvisoId, input.UserName);
        }
    }
    public class UpdateEstadoAvisoUseCase : CrudAviso, IUpdateEstadoAvisoUseCase
    {
        public UpdateEstadoAvisoUseCase(IAvisoSucessOutputPort output, IAvisoRepository repository, IAvisoService service) : base(output, repository, service)
        {
        }

        public Task Execute(UpdateEstadoAvisoInput input)
        {
            return UpdateEstado(input.AvisoId, input.Activo);
        }
        
    }
}
