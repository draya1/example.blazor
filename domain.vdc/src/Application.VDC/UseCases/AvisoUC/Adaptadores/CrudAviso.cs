﻿using Domain.VDC;
using Domain.VDC.Avisos;
using System.Threading.Tasks;

namespace Application.VDC.UseCases.AvisoUC.Adaptadores
{
    public class CrudAviso 
    {
        private readonly IAvisoService _service;
        private readonly IAvisoSucessOutputPort _output;
        private readonly IAvisoRepository _repository;

        public CrudAviso(IAvisoSucessOutputPort output, IAvisoRepository repository, IAvisoService service)
        {
            _output = output;
            _repository = repository;
            _service = service;
        }

        public async Task Create(Aviso aviso)
        {

            var item = _service.New(aviso);
            await _repository.Add(item);
            _output.Standard(new AvisoSuccessOutput(item.Id, true));
            
        }

        public async Task UpdateLeido(int avisoId, string userName)
        {
            try
            {
                var aviso = await _repository.GetWithUsersBy(avisoId);
                aviso.LeidoBy(userName);
                await _repository.Update(aviso);

                _output.Standard(new AvisoSuccessOutput(avisoId,true));

            }
            catch (NotFoundException ex)
            {

                _output.NotFound(ex.Message);
            }
        }

        public async Task UpdateEstado(int avisoId, bool activo)
        {
            try
            {
                var aviso = await _repository.GetWithUsersBy(avisoId);
                aviso.Activar(activo);
                await _repository.Update(aviso);                
                _output.Standard(new AvisoSuccessOutput(avisoId,true));

            }
            catch (NotFoundException ex)
            {

                _output.NotFound(ex.Message);
            }
        }
    }
}
