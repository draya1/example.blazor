﻿using Domain.VDC.Avisos;
using System.Threading.Tasks;

namespace Application.VDC.UseCases.AvisoUC.Adaptadores
{
    public class CreateAvisoUseCase : CrudAviso, ICreateAvisoUseCase
    {
        
        public CreateAvisoUseCase(IAvisoSucessOutputPort output, IAvisoRepository repository, IAvisoService service) : base(output, repository, service)
        {            
        }

        public Task Execute(CreateAvisoInput input)
        {            
            
            return Create(input.Aviso);
        }
    }
}
