﻿namespace Application.VDC.UseCases
{
    public interface IOutputPortInvalid
    {
        void NotValid(string message);
    }
}
