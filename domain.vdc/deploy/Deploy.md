# Web Api VDC

## Versión

1.8.2


## Prerequisitos para generar binarios

Tener instalada en el equipo local las versiones dotnet-core SDK 3.1

[https://dotnet.microsoft.com/download/dotnet-core/3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1 "dotnet core sdk 3.1")

Comprobar versiones instaladas abriendo terminal y lanzando el siguiente comando:
	
	dotnet --version

## Compilar

Ejecutad desde un terminal el comando siguiente desde la carpeta **/src**:

	dotnet build Api.VDC -c Release

## Crear binarios para publicación

Ejecutada desde un terminal el comando siguiente desde la carpeta **/src**:  

[version] => versión actual del artefacto.
Ignorar advertencias/warnings

Preproducción

	dotnet publish Api.VDC -c Release -o ../deploy/Api.VDC-[version] --no-restore /p:EnvironmentName=Staging

Producción

	dotnet publish Api.VDC -c Release -o ../deploy/Api.VDC-[version] --no-restore

## Configuraciones

Preproducción

	Editar el fichero **'/deploy/Api.VDC-[version]/appsettings.Staging.json'**
	 

Producción

	Editar el fichero **'/deploy/Api.VDC-[version]/appsettings.Production.json'**
	

Informar los siguientes parámetros entre corchetes, con los valores del entorno a desplegar:

*DefaultConnection* : "Data Source=[sqlServer];Initial Catalog=[Databaseweb];Persist Security Info=True;User ID=[user];Password=[password]"

*DataMemory*

- *active* : "false"

## Prerequisitos Deploy

- Base de datos DatabaseWeb versión 1.10.0.20 con datos iniciales.

## Deploy

**Carpeta origen**. Carpeta local generada al crear binarios para publicación: **/deploy/Api.VDC-[version]**

**Carpeta de destino**. Carpeta en el servidor dónde se ubican los archivos de la aplicación **'services'** del site del Portal en IIS.

**Pasos para copiar desde origen a destino**
	
1. Copiar archivo 'app_offline.htm' en la carpeta de destino. (Esto hará que se detenga el site, mostrando el contenido html del fichero app_offline, permitiendo reemplazar archivos en el destino.)

2. Copiar todos los archivos de la carpeta origen a la carpeta destino.

3. Borrar archivo 'app_offline.htm' de la carpeta destino. (Esto hará que se inicie de nuevo el site)

## Testear despligue

1. Abrir navegador escribir la siguiente url =>  http://[dnsPublica]/services/api/tipospoliza
2. Deberá mostrarse json con los tipos de poliza.