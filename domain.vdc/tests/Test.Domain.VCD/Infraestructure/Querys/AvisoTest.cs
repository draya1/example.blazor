﻿using Domain.VDC.Avisos;
using Infrastructure.VDC.Querys;
using Infrastructure.VDC.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System.Linq;

namespace Test.Domain.VCD.Infraestructure.Querys
{
    public class AvisoTest
    {
        private IAvisoQuerys _querys;
        private static readonly ILoggerFactory logger = LoggerFactory.Create(builder => { builder.AddConsole(); });

        [SetUp]
        public void Setup()
        {



            #region Memory
            var options = new DbContextOptionsBuilder<CedContext>()
              .UseInMemoryDatabase(databaseName: "memory")
              .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options;

            var context = new CedContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            #endregion


            #region DataBase
            //var configuration = TestHelper.GetIConfigurationRoot(TestContext.CurrentContext.TestDirectory);            
            //var password = configuration["DbPassword"];
            //var options = new DbContextOptionsBuilder<CedContext>()
            //   .UseLoggerFactory(logger)
            //   .UseSqlServer(configuration.GetConnectionString("DefaultConnection").Replace("<DbPassword>", configuration["DbPassword"]))
            //   .Options;

            //var context = new CedContext(options);
            #endregion


            _querys = new AvisoQuerys(context);
        }

        [Test]
        public void Test_getAvisos()
        {
            var result = _querys.GetAvisos().ToList();

            Assert.AreEqual(5, result.Count());
        }

        [Test]
        public void Test_getCabeceraAvisos()
        {
            var result = _querys.GetCabecerasAvisos().ToList();

            Assert.AreEqual(7, result.Count());
        }

        [Test]
        public void Test_getCabeceraAvisos_filtrado()
        {
            //todos del usuario alice del tipo inmediato de cualquier hora
            var result = _querys.GetCabecerasAvisos(null, null, 1,"alice",null,null).ToList();

            Assert.AreEqual(2, result.Count());

            //todos los activos del usuario alice del tipo inmediato entre fechas concretas
            result = _querys.GetCabecerasAvisos(true, false, 1, "alice", new System.DateTime(2020, 1, 1, 1, 11, 11), new System.DateTime(2020,1,1,1,11,11)).ToList();

            Assert.AreEqual(1, result.Count());


            //todos los activos del usuario alice del tipo inmediato entre fechas concretas
            result = _querys.GetCabecerasAvisos(false, true, 1, "alice", null, null).ToList();

            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Test_getAvisosCategorias()
        {
            var result = _querys.GetCategoriasAvisos().ToList();

            Assert.AreEqual(3, result.Count());
        }
        [Test]
        public void Test_getAvisosTipos()
        {
            var result = _querys.GetTiposAvisos().ToList();

            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void Test_getCabecerasAvisosFiltered()
        {
            var result = _querys.GetCabecerasAvisosFiltered("inicio");

            Assert.AreEqual(2, result.Count());
        }
    }
}
