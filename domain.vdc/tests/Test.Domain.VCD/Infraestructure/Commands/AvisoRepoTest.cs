﻿using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test.Domain.VCD.Infraestructure.Commands
{
    public class AvisoRepoTest
    {
        private IAvisoRepository _repository;
        
        [SetUp]
        public void Setup()
        {

            var options = new DbContextOptionsBuilder<CedContext>()
                .UseInMemoryDatabase(databaseName: "memory")
                .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            var context = new CedContext(options);

            _repository = new AvisoRepository(context);
        }

        [Test]
        public async Task Test_AddAsync()
        {
            IAvisoFactory factory;
            factory = new EntityFactory();

            var users = new List<AvisoUsuario>();
            users.Add(new AvisoUsuario(0, 0, "user02", null));
            var createDate = DateTime.Now.AddDays(10);
            var fromDate = createDate.AddHours(1);
            var toDate = createDate.AddHours(2);
            var data = factory.NewAviso(1, 1, "Titulo 01", "Texto 01", createDate, fromDate, toDate, true, users);

            //create
            await _repository.Add(data);
            var record = await _repository.GetBy(data.Id);



            var result = (Aviso)record;
            Assert.IsTrue(result.Id > 0);

            factory = new EntityFactory();
            var data2 = factory.NewAviso(2, 3, "Titulo 02", "Texto 02", createDate, fromDate, toDate, true, users);

            //create
            await _repository.Add(data2);
            var record2 = await _repository.GetBy(data2.Id);

            var result2 = (Aviso)record2;
            Assert.IsTrue(result2.Id > 1);

        }
    }
}
