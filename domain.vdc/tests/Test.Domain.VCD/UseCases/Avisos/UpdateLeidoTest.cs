﻿using Api.VDC.UseCases.Avisos;
using Api.VDC.UseCases.Avisos.Update;
using Application.VDC.UseCases.AvisoUC;
using Application.VDC.UseCases.AvisoUC.Adaptadores;
using Domain.VDC.Avisos;
using Domain.VDC.ValueObjects;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Querys;
using Infrastructure.VDC.Repositories;
using Infrastructure.VDC.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Domain.VCD.UseCases.Avisos
{
    public class UpdateLeidoTest
    {
        private IAvisoRepository _repository;
        private IUpdateLeidoAvisoUseCase _useCase;
        private IAvisoSucessOutputPort _presentador;
        private IAvisoService _service;
        private IAvisoFactory _factory;
        private IAvisoPersistenceValidator _validator;

        [SetUp]
        public void Setup()
        {

            CultureInfo.CurrentCulture = new CultureInfo("es-ES", false);

            var options = new DbContextOptionsBuilder<CedContext>()
               .UseInMemoryDatabase(databaseName: "memory")
               .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
               .Options;

            var context = new CedContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            _repository = new AvisoRepository(context);

            _presentador = new AvisoPresenter();
            _factory = new EntityFactory();
            _validator = new AvisoPersistenceValidator(context);
            _service = new AvisoService(_factory, _validator);

            _useCase = new UpdateLeidoAvisoUseCase(_presentador, _repository, _service);
        }

        [Test]
        public async Task Test_Update_Leido_AvisoAsync()
        {            
            var Servicio = (Aviso)await _repository.GetBy(1);
            
            

            var request = new UpdateLeidoAvisoInput(1,"alice");
            
            await _useCase.Execute(request);            
            var result = (Aviso)await _repository.GetBy(1);
            var leido = result.AvisosUsuarios.FirstOrDefault(av => av.UserName == "alice")?.FechaLeido;

            Assert.AreEqual(1, result.Id);
            Assert.IsNotNull(leido);
            

        }
    }
}
