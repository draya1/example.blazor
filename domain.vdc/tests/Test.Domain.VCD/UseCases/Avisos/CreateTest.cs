﻿using Api.VDC.UseCases.Avisos;
using Api.VDC.UseCases.Avisos.Create;
using Application.VDC.UseCases.AvisoUC;
using Application.VDC.UseCases.AvisoUC.Adaptadores;
using Domain.VDC;
using Domain.VDC.Avisos;
using Domain.VDC.ValueObjects;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Repositories;
using Infrastructure.VDC.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Domain.VCD.UseCases.Avisos
{
    public class CreateTest
    {

        private IAvisoRepository _repository;                
        private ICreateAvisoUseCase _useCase;
        private IAvisoSucessOutputPort _presentador;
        private IAvisoService _service;
        private IAvisoFactory _factory;                
        private IAvisoPersistenceValidator _validator;
        private CedContext context;

        [SetUp]
        public void Setup()
        {
            CultureInfo.CurrentCulture = new CultureInfo("es-ES", false);
            
            var options = new DbContextOptionsBuilder<CedContext>()
               .UseInMemoryDatabase(databaseName: "memory")
               .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
               .Options;

            context = new CedContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            _repository = new AvisoRepository(context);                                    

            _presentador = new AvisoPresenter();
            _factory = new EntityFactory();
            _validator = new AvisoPersistenceValidator(context);
            _service = new AvisoService(_factory, _validator);

            _useCase = new CreateAvisoUseCase(_presentador, _repository, _service);
        }

        [Test]
        public async Task Test_Create_AvisoAsync()
        {


            var users = new List<AvisoUsuario> { new AvisoUsuario(0, 0, userName: "alice", null) };
            var request = new CreateAvisoInput(new Aviso(1,1,DateTime.Now, DateTime.Now,DateTime.Now.AddDays(1),"Titulo nuevo","Texto nuevo",true,users));
            await _useCase.Execute(request);

            var result = context.Avisos.FirstOrDefault(av => av.Titulo == "Titulo nuevo");

            Assert.AreEqual(1, result.AvisoCategoriaId);
            Assert.AreEqual(1, result.AvisoTipoId);
            Assert.AreEqual("Titulo nuevo", result.Titulo);
            Assert.AreEqual("Texto nuevo", result.Texto);


        }

        
        

    }
}
