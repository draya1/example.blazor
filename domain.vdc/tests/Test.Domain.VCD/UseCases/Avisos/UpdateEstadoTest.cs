﻿using Api.VDC.UseCases.Avisos;
using Application.VDC.UseCases.AvisoUC;
using Application.VDC.UseCases.AvisoUC.Adaptadores;
using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Repositories;
using Infrastructure.VDC.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System.Globalization;
using System.Threading.Tasks;

namespace Test.Domain.VCD.UseCases.Avisos
{
    public class UpdateEstadoTest
    {
        private IAvisoRepository _repository;
        private IUpdateEstadoAvisoUseCase _useCase;
        private IAvisoSucessOutputPort _presentador;
        private IAvisoService _service;
        private IAvisoFactory _factory;
        private IAvisoPersistenceValidator _validator;

        [SetUp]
        public void Setup()
        {

            CultureInfo.CurrentCulture = new CultureInfo("es-ES", false);

            var options = new DbContextOptionsBuilder<CedContext>()
               .UseInMemoryDatabase(databaseName: "memory")
               .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
               .Options;

            var context = new CedContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            _repository = new AvisoRepository(context);

            _presentador = new AvisoPresenter();
            _factory = new EntityFactory();
            _validator = new AvisoPersistenceValidator(context);
            _service = new AvisoService(_factory, _validator);

            _useCase = new UpdateEstadoAvisoUseCase(_presentador, _repository, _service);
        }

        [Test]
        public async Task Test_Update_Leido_AvisoAsync()
        {            

            var request = new UpdateEstadoAvisoInput(1, false);

            await _useCase.Execute(request);
            var result = (Aviso)await _repository.GetBy(1);

            Assert.AreEqual(false, result.Activo);            


        }
    }
}
