﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;
using System;

namespace Test.Domain.VCD.ValueObjects
{
    public class NameTest
    {
        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void Test_name_validation()
        {
            var ex = Assert.Throws<NameValidationException>(() => new Name(""));
            Assert.That(ex.Message == "The 'Name' is required.", ex.Message);

            ex = Assert.Throws<NameValidationException>(() => new Name(new String('X', 51)));
            Assert.That(ex.Message == "The 'Name' supports a maximum of 50 characters.", ex.Message);
        }

        [Test]
        public void Test_name_comparer()
        {
            //correct names comparer
            var name1 = new Name("Manuel");
            var name2 = new Name("Manuel");

            Assert.IsTrue(name1.ToString() == "Manuel");
            Assert.IsTrue(name2.ToString() == "Manuel");

            Assert.AreEqual(name1, name2);
        }

    }
}
