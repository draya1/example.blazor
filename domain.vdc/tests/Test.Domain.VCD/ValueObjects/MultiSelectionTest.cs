﻿using Domain.VDC.ValueObjects;
using NUnit.Framework;
using System.Linq;

namespace Test.Domain.VCD.ValueObjects
{
    public class MultiSelectionTest
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_success()
        {

            MultiSelection data = new MultiSelection(string.Empty);
            Assert.AreEqual(string.Empty, data.ToString());
            Assert.AreEqual(0, data.ToList().Count);

            data = new MultiSelection(new int[] { 1 });
            Assert.AreEqual("1", data.ToString());
            Assert.AreEqual(1, data.ToList().Count);
            
            data = new MultiSelection(new int[] { 1, 2, 5 });
            Assert.AreEqual("1;2;5", data.ToString());
            Assert.AreEqual("1", data.ToList().FirstOrDefault().ToString());
            Assert.AreEqual("5", data.ToList().LastOrDefault().ToString());
            Assert.AreEqual(3, data.ToList().Count);

            data = new MultiSelection(new int[] { });
            Assert.AreEqual(string.Empty, data.ToString());
            Assert.AreEqual(0, data.ToList().Count);

            data = new MultiSelection();
            Assert.AreEqual(null, data.ToString());
            Assert.AreEqual(0, data.ToList().Count);

            data = new MultiSelection("");
            Assert.AreEqual("", data.ToString());
            Assert.AreEqual(0, data.ToList().Count);

            data = new MultiSelection("3");
            Assert.AreEqual("3", data.ToString());
            Assert.AreEqual("3", data.ToList().FirstOrDefault().ToString());
            Assert.AreEqual(1, data.ToList().Count);

            data = new MultiSelection("3;22");
            Assert.AreEqual("3;22", data.ToString());
            Assert.AreEqual("3", data.ToList().FirstOrDefault().ToString());
            Assert.AreEqual("22", data.ToList().LastOrDefault().ToString());
            Assert.AreEqual(2, data.ToList().Count);

            data = new MultiSelection("3;22;");
            Assert.AreEqual("3;22;", data.ToString());
            Assert.AreEqual("3", data.ToList().FirstOrDefault().ToString());
            Assert.AreEqual("22", data.ToList().LastOrDefault().ToString());
            Assert.AreEqual(2, data.ToList().Count);


        }


        [Test]
        public void Test_multiselection_validation()
        {
            var ex = Assert.Throws<MultiSelectionValidationException>(() => new MultiSelection("a"));
            Assert.That(ex.Message == "The 'MultiSelection' value must be an integer.", ex.Message);

            var ex2 = Assert.Throws<MultiSelectionValidationException>(() => new MultiSelection("1;a"));
            Assert.That(ex.Message == "The 'MultiSelection' value must be an integer.", ex2.Message);
        }
    }
}
