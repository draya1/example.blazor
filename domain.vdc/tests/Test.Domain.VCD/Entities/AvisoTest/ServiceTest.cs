﻿using Domain.VDC;
using Domain.VDC.Avisos;
using Infrastructure.VDC.Data;
using Infrastructure.VDC.Repositories;
using Infrastructure.VDC.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Test.Domain.VCD.Entities.AvisoTest
{
    public class ServiceTest
    {
        private IAvisoService _service;
        private IAvisoFactory _factory;
        private IAvisoPersistenceValidator _validator;
        private IAvisoRepository _repository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<CedContext>()
              .UseInMemoryDatabase(databaseName: "memory")
              .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options;

            var context = new CedContext(options);
            context.Database.EnsureCreated();

            _repository = new AvisoRepository(context);
            _factory = new EntityFactory();
            _validator = new AvisoPersistenceValidator(context);
            _service = new AvisoService(_factory, _validator);

        }

        [Test]
        public void Test_set_item_sucess()
        {
            var users = new List<AvisoUsuario>();
            users.Add(new AvisoUsuario(1, 1, "user01", null));
            var createDate = DateTime.Now;
            var fromDate = createDate.AddHours(1);
            var toDate = createDate.AddHours(2);
            var item = _factory.NewAviso(1, 1, "Titulo 01", "Texto 01", createDate, fromDate, toDate, true, users);
            var data = (Aviso)_service.Set(item, true, "user01");
            
            Assert.AreEqual(1,data.AvisoCategoriaId);
            Assert.AreEqual(1, data.AvisoTipoId);
            Assert.AreEqual("Titulo 01", data.Titulo);
            Assert.AreEqual("Texto 01", data.Texto);
            Assert.AreEqual(true, data.Activo);
            Assert.AreEqual(createDate, data.FechaCreacion);
            Assert.AreEqual(fromDate, data.FechaDesde);
            Assert.AreEqual(toDate, data.FechaHasta);
            CollectionAssert.AreEqual(users, data.AvisosUsuarios);

        }

        [Test]
        public void Test_new_item_sucess()
        {
            var users = new List<AvisoUsuario>();
            users.Add(new AvisoUsuario(0, 0, "user02", null));
            var createDate = DateTime.Now.AddDays(10);
            var fromDate = createDate.AddHours(1);
            var toDate = createDate.AddHours(2);            
            var data = (Aviso)_service.New(2, 3, "Titulo 02", "Texto 02", createDate, fromDate, toDate, true, users);

            
            Assert.AreEqual(2, data.AvisoTipoId);
            Assert.AreEqual(3, data.AvisoCategoriaId);
            Assert.AreEqual("Titulo 02", data.Titulo);
            Assert.AreEqual("Texto 02", data.Texto);
            Assert.AreEqual(true, data.Activo);
            Assert.AreEqual(createDate, data.FechaCreacion);
            Assert.AreEqual(fromDate, data.FechaDesde);
            Assert.AreEqual(toDate, data.FechaHasta);
            CollectionAssert.AreEqual(users, data.AvisosUsuarios);

        }

        [Test]
        public void Test_set_validation_error()
        {
            
            var item = _factory.NewAviso(null, null, "","",null,null,null,null,null);
            Assert.Throws<DomainException>(() => _service.Set(item, true, ""));

        }

        [Test]
        public void Test_new_validation_error()
        {
            
            Assert.Throws<DomainException>(() => _service.New(null, null, "", "", null, null, null, null, null));

        }
    }
}
